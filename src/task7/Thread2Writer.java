import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileReader;
import java.io.PipedWriter;

public class Thread2Writer implements Runnable {

	PipedWriter pw;

	String name = null;

	public Thread2Writer(String name, PipedWriter pw) {
		this.name = name;
		this.pw = pw;
	}

	public void run() {
		try {

			FileReader reader = new FileReader("C:\\Users\\Taras\\eclipse-workspace\\HWThreads1\\task7\\1.txt");

			StringBuilder stringBuilder = new StringBuilder();

			int data = reader.read();

			while (data != -1) {

				stringBuilder.append(((char) data));

				data = reader.read();
			}
			
				pw.write(stringBuilder.toString());
				pw.flush();
				Thread.sleep(2000);
		} catch (Exception e) {
			System.out.println(" PipeThread Exception: " + e);
		}
	}

}
