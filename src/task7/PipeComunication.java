import java.io.PipedReader;
import java.io.PipedWriter;

public class PipeComunication {


	public PipeComunication() {
		try {
			PipedReader pr = new PipedReader();
			PipedWriter pw = new PipedWriter();

			pw.connect(pr);

			Thread thread1 = new Thread(new Thread1Reader("ReaderThread", pr));

			Thread thread2 = new Thread(new Thread2Writer("WriterThread", pw));

			thread2.start();
			thread1.start();

		
		} catch (Exception e) {
			System.out.println("PipeThread Exception: " + e);
		} 
	}
	
	public static void main(String[] args) {
		new PipeComunication();
	
	}
}
