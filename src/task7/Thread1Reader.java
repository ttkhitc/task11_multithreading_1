import java.io.PipedReader;

public class Thread1Reader implements Runnable {

	PipedReader pr;

	String name = null;

	public Thread1Reader(String name, PipedReader pr) {
		this.name = name;
		this.pr = pr;
	}

	public void run() {
		try {
//			while (true) {
//				char c = (char) pr.read(); // read a char
//				if (c != -1) { 
//					System.out.print(c);
//				}
//			}
			char c = 0;
			while (c != -1) {
			c= 	(char) pr.read();
					System.out.print(c);
				
			}
		} catch (Exception e) {
			System.out.println(" PipeThread Exception: " + e);
		}
	}

}
