import java.time.LocalDateTime;

public class PingPong {

	private static Object pingpong = new Object();

	public static void pingPong() {

		Thread ping = new Thread(() -> {

			synchronized (pingpong) {

				for (int i = 1; i <= 10; i++) {

					try {

						pingpong.wait();

					} catch (InterruptedException e) {

					}

					System.out.println("Ping");

					pingpong.notify();
				}

			}
		});

		Thread pong = new Thread(() -> {

			synchronized (pingpong) {

				for (int i = 1; i <= 10; i++) {

					pingpong.notify();

					try {

						pingpong.wait();

					} catch (InterruptedException e) {

					}

					System.out.println("Pong");
				}

			}
		});
		ping.start();
		pong.start();
	}

	public static void main(String[] args) {

		pingPong();
	}

}
